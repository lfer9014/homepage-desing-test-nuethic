window.addEventListener('load', function(){
    // new Glider(document.querySelector('.carousel-list'), {
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     dots: '.carousel-index',
    //     arrows: {
    //         prev: '.glider-prev',
    //         next: '.glider-next'
    //     }
    // });

    const gliders = document.querySelectorAll("[id^=glider]");

    gliders.forEach((glide)=> {
        const glider = document.querySelector(`#${glide.id} .carousel-list`);
        const gliderPrev = document.querySelector(`#${glide.id} .glider-prev`);
        const gliderNext = document.querySelector(`#${glide.id} .glider-next`);
        const gliderDots = document.querySelector(`#${glide.id}`).nextElementSibling;

        new Glider(glider, {
            slidesToShow: 1,
            dots: gliderDots,
            arrows: {
              prev: gliderPrev,
              next: gliderNext
            }
        });
    });
});